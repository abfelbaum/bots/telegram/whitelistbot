﻿FROM registry.git.abfelbaum.dev/abfelbaum/images/dotnet/runtime
WORKDIR /app
COPY publish/ .
ENTRYPOINT ["dotnet", "TelegramWhiteListBot.dll"]