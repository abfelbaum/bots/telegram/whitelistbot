using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Options;
using TelegramWhiteListBot.Models;

namespace TelegramWhiteListBot.Commands;

public class WhiteListCommandSettings : CommandSettings
{
    private readonly TelegramOptions _options;

    public WhiteListCommandSettings(IBotService botService, IScopeMatchingService scopeMatchingService,
        IOptions<TelegramOptions> options) : base(botService, scopeMatchingService)
    {
        _options = options.Value;
    }

    public override CommandDocumentation Documentation { get; } = new("whitelist")
    {
        Usages = { "whitelist <username>" },
        ShortDescription = "Whitelists an user",
        LongDescription = "Whitelists an user on the specified minecraft server"
    };

    public override IEnumerable<CommandHandler> Handlers => new[]
        { new CommandHandler<WhiteListCommandHandler>(CommandScope.Chat(_options.ChatId)) };
}