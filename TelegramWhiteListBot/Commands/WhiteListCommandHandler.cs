using System.Net;
using System.Text;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramWhiteListBot.Interfaces;
using TelegramWhiteListBot.Models;

namespace TelegramWhiteListBot.Commands;

public class WhiteListCommandHandler : ITelegramUpdate<Message>
{
    private readonly IMinecraftApiService _apiService;
    private readonly ICommandHelpGenerator _helpGenerator;
    private readonly ITelegramBotClient _botClient;
    private readonly TelegramOptions _options;

    public WhiteListCommandHandler(IMinecraftApiService apiService, ICommandHelpGenerator helpGenerator,
        ITelegramBotClient botClient, IOptions<TelegramOptions> options)
    {
        _apiService = apiService;
        _helpGenerator = helpGenerator;
        _botClient = botClient;
        _options = options.Value;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.Chat.Id != _options.ChatId)
        {
            return;
        }
        
        
        string[] arguments = message.GetCommandArguments().Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
        
        if (arguments.Length != 1 || string.IsNullOrWhiteSpace(arguments.FirstOrDefault()))
        {
            StringBuilder? helpText = await _helpGenerator.Generate("whitelist", message.Chat, message.From, cancellationToken);

            await _botClient.SendTextMessageAsync(message.Chat.Id, helpText?.ToString() ?? "No", ParseMode.Html, replyToMessageId: message.MessageId, cancellationToken: cancellationToken);
            
            return;
        }

        WhiteListStatus status = await _apiService.AddToWhiteList(arguments.First());

        string user = WebUtility.HtmlEncode(arguments.First());
        string text = status switch
        {
            WhiteListStatus.Success => $"Added <code>{user}</code> to whitelist",
            WhiteListStatus.Failed => $"Failed to add <code>{user}</code> to whitelist",
            WhiteListStatus.Duplicate => $"<code>{user}</code> is already whitelisted",
            WhiteListStatus.NoWhiteList => "Whitelist is not enabled",
            _ => throw new ArgumentOutOfRangeException()
        };

        await _botClient.SendTextMessageAsync(message.Chat.Id, text, ParseMode.Html, replyToMessageId: message.MessageId, cancellationToken: cancellationToken);
    }
}