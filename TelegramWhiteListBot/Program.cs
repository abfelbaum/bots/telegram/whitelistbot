﻿using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Configuration;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Hosting;
using Abfelbaum.Telegram.Bot.Framework.Polling;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Minecraft.Api;
using TelegramWhiteListBot.Commands;
using TelegramWhiteListBot.Interfaces;
using TelegramWhiteListBot.Models;
using TelegramWhiteListBot.Services;

TelegramApplicationBuilder<DefaultContext> builder = TelegramApplication<DefaultContext>.CreateBuilder(new TelegramApplicationOptions
{
    Args = args,
    ApplicationName = "Minecraft whitelist bot"
});

builder.Host.UseConsoleLifetime();

builder.Logging.AddFilter("System.Net.Http.HttpClient", LogLevel.Warning);

builder.Configuration.AddCommandLine(args);
builder.Configuration.AddEnvironmentVariables("WhiteListBot_");
builder.Configuration.AddJsonFile("appsettings.json", true);
builder.Configuration.AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", true);

builder.Services.Configure<TelegramOptions>(builder.Configuration.GetSection("Telegram"));
builder.Services.Configure<ApiOptions>(builder.Configuration.GetSection("Api"));
builder.Services.Configure<BotOptions>(builder.Configuration.GetSection("Bot"));
builder.Services.AddBot(builder.Configuration.GetSection("Bot"));

builder.Services.AddCommandUpdater();
builder.Services.AddPolling();

builder.Services.AddConcurrentUpdates();

builder.Services.AddTransient(_ => (IServerApiAsync) new ServerApi(builder.Configuration.GetValue<string>("Api:Uri")));

builder.Services.AddTransient<IMinecraftApiService, MinecraftApiService>();


builder.AddHelpCommand();
builder.AddCommand<WhiteListCommandSettings>();

TelegramApplication<DefaultContext> app = builder.Build();

app.UseConcurrentUpdates();
app.UseUpdateHandling();

await app.RunAsync().ConfigureAwait(false);