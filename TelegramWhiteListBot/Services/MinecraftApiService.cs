using Microsoft.Extensions.Options;
using Minecraft.Api;
using TelegramWhiteListBot.Interfaces;
using TelegramWhiteListBot.Models;

namespace TelegramWhiteListBot.Services;

public class MinecraftApiService : IMinecraftApiService
{
    private readonly IServerApiAsync _serverApi;
    private readonly ApiOptions _options;

    public MinecraftApiService(IServerApiAsync serverApi, IOptions<ApiOptions> options)
    {
        _serverApi = serverApi;
        _options = options.Value;
    }

    public async Task<WhiteListStatus> AddToWhiteList(string name)
    {
        object response = await _serverApi.PostV1ServerWhitelistAsync(_options.Token, name: name);

        if (response is not string result)
        {
            throw new Exception("Failed");
        }

        return result switch
        {
            "success" => WhiteListStatus.Success,
            "Error: duplicate entry" => WhiteListStatus.Duplicate,
            "No whitelist" => WhiteListStatus.NoWhiteList,
            _ => WhiteListStatus.Failed
        };
    }
}