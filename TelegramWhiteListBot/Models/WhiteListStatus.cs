namespace TelegramWhiteListBot.Models;

public enum WhiteListStatus
{
    Success,
    Failed,
    Duplicate,
    NoWhiteList
}