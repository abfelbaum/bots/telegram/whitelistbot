using Telegram.Bot.Types;

namespace TelegramWhiteListBot.Models;

public class ApiOptions
{
    public Uri Endpoint { get; set; } = null!;
    public string Token { get; set; } = null!;
}