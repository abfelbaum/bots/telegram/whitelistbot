using TelegramWhiteListBot.Models;

namespace TelegramWhiteListBot.Interfaces;

public interface IMinecraftApiService 
{
    public Task<WhiteListStatus> AddToWhiteList(string name);
}