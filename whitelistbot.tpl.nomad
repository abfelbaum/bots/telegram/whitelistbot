# This should best be equal to project name, and also the binary name without extension. This defines the $${NOMAD_JOB_NAME}
job "whitelistbot" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "whitelistbot" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "whitelistbot"
    }

    task "whitelistbot" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
      }

      vault {
        env      = false
        policies = ["whitelistbot"]
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/whitelistbot" }}
          WhiteListBot_Bot__Token={{.Data.data.BOT_TOKEN}}
          WhiteListBot_Api__Uri={{.Data.data.API_URI}}
          WhiteListBot_Api__Token={{.Data.data.API_TOKEN}}
          WhiteListBot_Telegram__ChatId={{.Data.data.CHAT_ID}}
{{ end }}
        EOF
      }
    }
  }
}