# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2022-11-25)


### Features

* **ci:** Implement standard ci ([fca459d](https://git.abfelbaum.dev/abfelbaum/bots/telegram/whitelistbot/commit/fca459d97988904d4b21c5e26f5696212f2db5f6))


### Bug Fixes

* chat id handling ([3a78d4d](https://git.abfelbaum.dev/abfelbaum/bots/telegram/whitelistbot/commit/3a78d4dbc627474d9a413be3f7a9c1abea43fc87))
