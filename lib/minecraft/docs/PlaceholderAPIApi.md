# Minecraft.Api.PlaceholderAPIApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**PostV1PlaceholdersReplace**](PlaceholderAPIApi.md#postv1placeholdersreplace) | **POST** /v1/placeholders/replace | Process a string using PlaceholderAPI |

<a name="postv1placeholdersreplace"></a>
# **PostV1PlaceholdersReplace**
> Object PostV1PlaceholdersReplace (string message, string? key = null, string? uuid = null)

Process a string using PlaceholderAPI

Process a string using PlaceholderAPI

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1PlaceholdersReplaceExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlaceholderAPIApi(config);
            var message = "message_example";  // string | 
            var key = "key_example";  // string? |  (optional) 
            var uuid = "uuid_example";  // string? |  (optional) 

            try
            {
                // Process a string using PlaceholderAPI
                Object result = apiInstance.PostV1PlaceholdersReplace(message, key, uuid);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlaceholderAPIApi.PostV1PlaceholdersReplace: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1PlaceholdersReplaceWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Process a string using PlaceholderAPI
    ApiResponse<Object> response = apiInstance.PostV1PlaceholdersReplaceWithHttpInfo(message, key, uuid);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlaceholderAPIApi.PostV1PlaceholdersReplaceWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **message** | **string** |  |  |
| **key** | **string?** |  | [optional]  |
| **uuid** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

