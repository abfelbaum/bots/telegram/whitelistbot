# Minecraft.Model.Server

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**Motd** | **string** |  | [optional] 
**_Version** | **string** |  | [optional] 
**BukkitVersion** | **string** |  | [optional] 
**Tps** | **string** |  | [optional] 
**Health** | [**ServerHealth**](ServerHealth.md) |  | [optional] 
**BannedIps** | [**List&lt;ServerBan&gt;**](ServerBan.md) |  | [optional] 
**BannedPlayers** | [**List&lt;ServerBan&gt;**](ServerBan.md) |  | [optional] 
**WhitelistedPlayers** | [**List&lt;Whitelist&gt;**](Whitelist.md) |  | [optional] 
**MaxPlayers** | **int** |  | [optional] 
**OnlinePlayers** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

