# Minecraft.Model.OfflinePlayer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uuid** | **string** |  | [optional] 
**Whitelisted** | **bool** |  | [optional] 
**Banned** | **bool** |  | [optional] 
**Op** | **bool** |  | [optional] 
**Balance** | **double** |  | [optional] 
**LastPlayed** | **long** |  | [optional] 
**DisplayName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

