# Minecraft.Api.ServerApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**GetV1Ping**](ServerApi.md#getv1ping) | **GET** /v1/ping | pong! |
| [**GetV1Scoreboard**](ServerApi.md#getv1scoreboard) | **GET** /v1/scoreboard | Get information about the scoreboard objectives |
| [**GetV1ScoreboardWithName**](ServerApi.md#getv1scoreboardwithname) | **GET** /v1/scoreboard/{name} | Get information about a specific objective |
| [**GetV1Server**](ServerApi.md#getv1server) | **GET** /v1/server | Get information about the server |
| [**GetV1ServerWhitelist**](ServerApi.md#getv1serverwhitelist) | **GET** /v1/server/whitelist | Get the whitelist |
| [**GetV1Worlds**](ServerApi.md#getv1worlds) | **GET** /v1/worlds | Get information about all worlds |
| [**GetV1WorldsDownload**](ServerApi.md#getv1worldsdownload) | **GET** /v1/worlds/download | Downloads a ZIP compressed archive of all the worlds&#39; folders |
| [**GetV1WorldsWithUuid**](ServerApi.md#getv1worldswithuuid) | **GET** /v1/worlds/{uuid} | Get information about a specific world |
| [**GetV1WorldsWithUuidDownload**](ServerApi.md#getv1worldswithuuiddownload) | **GET** /v1/worlds/{uuid}/download | Downloads a ZIP compressed archive of the world&#39;s folder |
| [**PostV1ServerExec**](ServerApi.md#postv1serverexec) | **POST** /v1/server/exec | Executes a command on the server from the console, returning it&#39;s output. Be aware that not all command executors will properly send their messages to the CommandSender, though, most do. |
| [**PostV1ServerWhitelist**](ServerApi.md#postv1serverwhitelist) | **POST** /v1/server/whitelist | Update the whitelist |
| [**PostV1WorldsSave**](ServerApi.md#postv1worldssave) | **POST** /v1/worlds/save | Triggers a world save of all worlds |
| [**PostV1WorldsWithUuidSave**](ServerApi.md#postv1worldswithuuidsave) | **POST** /v1/worlds/{uuid}/save | Triggers a world save |

<a name="getv1ping"></a>
# **GetV1Ping**
> Object GetV1Ping (string? key = null)

pong!

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1PingExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // pong!
                Object result = apiInstance.GetV1Ping(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1Ping: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1PingWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // pong!
    ApiResponse<Object> response = apiInstance.GetV1PingWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1PingWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1scoreboard"></a>
# **GetV1Scoreboard**
> Scoreboard GetV1Scoreboard (string? key = null)

Get information about the scoreboard objectives

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1ScoreboardExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get information about the scoreboard objectives
                Scoreboard result = apiInstance.GetV1Scoreboard(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1Scoreboard: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1ScoreboardWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get information about the scoreboard objectives
    ApiResponse<Scoreboard> response = apiInstance.GetV1ScoreboardWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1ScoreboardWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**Scoreboard**](Scoreboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1scoreboardwithname"></a>
# **GetV1ScoreboardWithName**
> Objective GetV1ScoreboardWithName (string name, string? key = null)

Get information about a specific objective

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1ScoreboardWithNameExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var name = "name_example";  // string | The name of the objective to get
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get information about a specific objective
                Objective result = apiInstance.GetV1ScoreboardWithName(name, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1ScoreboardWithName: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1ScoreboardWithNameWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get information about a specific objective
    ApiResponse<Objective> response = apiInstance.GetV1ScoreboardWithNameWithHttpInfo(name, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1ScoreboardWithNameWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **name** | **string** | The name of the objective to get |  |
| **key** | **string?** |  | [optional]  |

### Return type

[**Objective**](Objective.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1server"></a>
# **GetV1Server**
> Server GetV1Server (string? key = null)

Get information about the server

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1ServerExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get information about the server
                Server result = apiInstance.GetV1Server(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1Server: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1ServerWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get information about the server
    ApiResponse<Server> response = apiInstance.GetV1ServerWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1ServerWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**Server**](Server.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1serverwhitelist"></a>
# **GetV1ServerWhitelist**
> List&lt;Whitelist&gt; GetV1ServerWhitelist (string? key = null)

Get the whitelist

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1ServerWhitelistExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get the whitelist
                List<Whitelist> result = apiInstance.GetV1ServerWhitelist(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1ServerWhitelist: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1ServerWhitelistWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get the whitelist
    ApiResponse<List<Whitelist>> response = apiInstance.GetV1ServerWhitelistWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1ServerWhitelistWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;Whitelist&gt;**](Whitelist.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1worlds"></a>
# **GetV1Worlds**
> List&lt;World&gt; GetV1Worlds (string? key = null)

Get information about all worlds

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1WorldsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get information about all worlds
                List<World> result = apiInstance.GetV1Worlds(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1Worlds: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1WorldsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get information about all worlds
    ApiResponse<List<World>> response = apiInstance.GetV1WorldsWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1WorldsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;World&gt;**](World.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1worldsdownload"></a>
# **GetV1WorldsDownload**
> Object GetV1WorldsDownload (string? key = null)

Downloads a ZIP compressed archive of all the worlds' folders

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1WorldsDownloadExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Downloads a ZIP compressed archive of all the worlds' folders
                Object result = apiInstance.GetV1WorldsDownload(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1WorldsDownload: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1WorldsDownloadWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Downloads a ZIP compressed archive of all the worlds' folders
    ApiResponse<Object> response = apiInstance.GetV1WorldsDownloadWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1WorldsDownloadWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1worldswithuuid"></a>
# **GetV1WorldsWithUuid**
> World GetV1WorldsWithUuid (string uuid, string? key = null)

Get information about a specific world

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1WorldsWithUuidExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var uuid = "uuid_example";  // string | The uuid of the world
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get information about a specific world
                World result = apiInstance.GetV1WorldsWithUuid(uuid, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1WorldsWithUuid: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1WorldsWithUuidWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get information about a specific world
    ApiResponse<World> response = apiInstance.GetV1WorldsWithUuidWithHttpInfo(uuid, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1WorldsWithUuidWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **uuid** | **string** | The uuid of the world |  |
| **key** | **string?** |  | [optional]  |

### Return type

[**World**](World.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1worldswithuuiddownload"></a>
# **GetV1WorldsWithUuidDownload**
> Object GetV1WorldsWithUuidDownload (string uuid, string? key = null)

Downloads a ZIP compressed archive of the world's folder

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1WorldsWithUuidDownloadExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var uuid = "uuid_example";  // string | The UUID of the World to download
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Downloads a ZIP compressed archive of the world's folder
                Object result = apiInstance.GetV1WorldsWithUuidDownload(uuid, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.GetV1WorldsWithUuidDownload: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1WorldsWithUuidDownloadWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Downloads a ZIP compressed archive of the world's folder
    ApiResponse<Object> response = apiInstance.GetV1WorldsWithUuidDownloadWithHttpInfo(uuid, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.GetV1WorldsWithUuidDownloadWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **uuid** | **string** | The UUID of the World to download |  |
| **key** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1serverexec"></a>
# **PostV1ServerExec**
> void PostV1ServerExec (string command, string? key = null, long? time = null)

Executes a command on the server from the console, returning it's output. Be aware that not all command executors will properly send their messages to the CommandSender, though, most do.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1ServerExecExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var command = "command_example";  // string | 
            var key = "key_example";  // string? |  (optional) 
            var time = 789L;  // long? |  (optional) 

            try
            {
                // Executes a command on the server from the console, returning it's output. Be aware that not all command executors will properly send their messages to the CommandSender, though, most do.
                apiInstance.PostV1ServerExec(command, key, time);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.PostV1ServerExec: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1ServerExecWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Executes a command on the server from the console, returning it's output. Be aware that not all command executors will properly send their messages to the CommandSender, though, most do.
    apiInstance.PostV1ServerExecWithHttpInfo(command, key, time);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.PostV1ServerExecWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **command** | **string** |  |  |
| **key** | **string?** |  | [optional]  |
| **time** | **long?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1serverwhitelist"></a>
# **PostV1ServerWhitelist**
> Object PostV1ServerWhitelist (string? key = null, string? uuid = null, string? name = null)

Update the whitelist

Possible responses are: `success`, `failed`, `Error: duplicate entry`, and `No whitelist`.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1ServerWhitelistExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 
            var uuid = "uuid_example";  // string? |  (optional) 
            var name = "name_example";  // string? |  (optional) 

            try
            {
                // Update the whitelist
                Object result = apiInstance.PostV1ServerWhitelist(key, uuid, name);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.PostV1ServerWhitelist: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1ServerWhitelistWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Update the whitelist
    ApiResponse<Object> response = apiInstance.PostV1ServerWhitelistWithHttpInfo(key, uuid, name);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.PostV1ServerWhitelistWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **uuid** | **string?** |  | [optional]  |
| **name** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1worldssave"></a>
# **PostV1WorldsSave**
> void PostV1WorldsSave (string? key = null)

Triggers a world save of all worlds

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1WorldsSaveExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Triggers a world save of all worlds
                apiInstance.PostV1WorldsSave(key);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.PostV1WorldsSave: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1WorldsSaveWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Triggers a world save of all worlds
    apiInstance.PostV1WorldsSaveWithHttpInfo(key);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.PostV1WorldsSaveWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1worldswithuuidsave"></a>
# **PostV1WorldsWithUuidSave**
> void PostV1WorldsWithUuidSave (string uuid, string? key = null)

Triggers a world save

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1WorldsWithUuidSaveExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ServerApi(config);
            var uuid = "uuid_example";  // string | The UUID of the World to save
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Triggers a world save
                apiInstance.PostV1WorldsWithUuidSave(uuid, key);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ServerApi.PostV1WorldsWithUuidSave: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1WorldsWithUuidSaveWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Triggers a world save
    apiInstance.PostV1WorldsWithUuidSaveWithHttpInfo(uuid, key);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ServerApi.PostV1WorldsWithUuidSaveWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **uuid** | **string** | The UUID of the World to save |  |
| **key** | **string?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

