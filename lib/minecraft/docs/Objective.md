# Minecraft.Model.Objective

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**DisplaySlot** | **string** |  | [optional] 
**Criterion** | **string** |  | [optional] 
**Scores** | [**List&lt;Score&gt;**](Score.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

