# Minecraft.Api.PlayerApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**DeleteV1ServerOps**](PlayerApi.md#deletev1serverops) | **DELETE** /v1/server/ops | Removes Op from a specific player |
| [**GetV1Players**](PlayerApi.md#getv1players) | **GET** /v1/players | Gets all currently online players |
| [**GetV1PlayersAll**](PlayerApi.md#getv1playersall) | **GET** /v1/players/all | Gets all players that have ever joined the server  |
| [**GetV1PlayersWithPlayeruuidWithWorlduuidInventory**](PlayerApi.md#getv1playerswithplayeruuidwithworlduuidinventory) | **GET** /v1/players/{playerUuid}/{worldUuid}/inventory | Gets a specific online player&#39;s Inventory in the specified world |
| [**GetV1PlayersWithUuid**](PlayerApi.md#getv1playerswithuuid) | **GET** /v1/players/{uuid} | Gets a specific online player by their UUID |
| [**GetV1ServerOps**](PlayerApi.md#getv1serverops) | **GET** /v1/server/ops | Get all op players |
| [**PostV1ServerOps**](PlayerApi.md#postv1serverops) | **POST** /v1/server/ops | Sets a specific player to Op |

<a name="deletev1serverops"></a>
# **DeleteV1ServerOps**
> void DeleteV1ServerOps (string? key = null, string? playerUuid = null)

Removes Op from a specific player

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class DeleteV1ServerOpsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var key = "key_example";  // string? |  (optional) 
            var playerUuid = "playerUuid_example";  // string? |  (optional) 

            try
            {
                // Removes Op from a specific player
                apiInstance.DeleteV1ServerOps(key, playerUuid);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.DeleteV1ServerOps: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteV1ServerOpsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Removes Op from a specific player
    apiInstance.DeleteV1ServerOpsWithHttpInfo(key, playerUuid);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.DeleteV1ServerOpsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **playerUuid** | **string?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1players"></a>
# **GetV1Players**
> List&lt;Player&gt; GetV1Players (string? key = null)

Gets all currently online players

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1PlayersExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Gets all currently online players
                List<Player> result = apiInstance.GetV1Players(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.GetV1Players: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1PlayersWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets all currently online players
    ApiResponse<List<Player>> response = apiInstance.GetV1PlayersWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.GetV1PlayersWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;Player&gt;**](Player.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1playersall"></a>
# **GetV1PlayersAll**
> List&lt;OfflinePlayer&gt; GetV1PlayersAll (string? key = null)

Gets all players that have ever joined the server 

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1PlayersAllExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Gets all players that have ever joined the server 
                List<OfflinePlayer> result = apiInstance.GetV1PlayersAll(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.GetV1PlayersAll: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1PlayersAllWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets all players that have ever joined the server 
    ApiResponse<List<OfflinePlayer>> response = apiInstance.GetV1PlayersAllWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.GetV1PlayersAllWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;OfflinePlayer&gt;**](OfflinePlayer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1playerswithplayeruuidwithworlduuidinventory"></a>
# **GetV1PlayersWithPlayeruuidWithWorlduuidInventory**
> List&lt;ItemStack&gt; GetV1PlayersWithPlayeruuidWithWorlduuidInventory (string playerUuid, string worldUuid, string? key = null)

Gets a specific online player's Inventory in the specified world

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1PlayersWithPlayeruuidWithWorlduuidInventoryExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var playerUuid = "playerUuid_example";  // string | UUID of the player
            var worldUuid = "worldUuid_example";  // string | UUID of the world
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Gets a specific online player's Inventory in the specified world
                List<ItemStack> result = apiInstance.GetV1PlayersWithPlayeruuidWithWorlduuidInventory(playerUuid, worldUuid, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.GetV1PlayersWithPlayeruuidWithWorlduuidInventory: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1PlayersWithPlayeruuidWithWorlduuidInventoryWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets a specific online player's Inventory in the specified world
    ApiResponse<List<ItemStack>> response = apiInstance.GetV1PlayersWithPlayeruuidWithWorlduuidInventoryWithHttpInfo(playerUuid, worldUuid, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.GetV1PlayersWithPlayeruuidWithWorlduuidInventoryWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **playerUuid** | **string** | UUID of the player |  |
| **worldUuid** | **string** | UUID of the world |  |
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;ItemStack&gt;**](ItemStack.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1playerswithuuid"></a>
# **GetV1PlayersWithUuid**
> Player GetV1PlayersWithUuid (string uuid, string? key = null)

Gets a specific online player by their UUID

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1PlayersWithUuidExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var uuid = "uuid_example";  // string | UUID of the player
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Gets a specific online player by their UUID
                Player result = apiInstance.GetV1PlayersWithUuid(uuid, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.GetV1PlayersWithUuid: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1PlayersWithUuidWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets a specific online player by their UUID
    ApiResponse<Player> response = apiInstance.GetV1PlayersWithUuidWithHttpInfo(uuid, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.GetV1PlayersWithUuidWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **uuid** | **string** | UUID of the player |  |
| **key** | **string?** |  | [optional]  |

### Return type

[**Player**](Player.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getv1serverops"></a>
# **GetV1ServerOps**
> List&lt;OfflinePlayer&gt; GetV1ServerOps (string? key = null)

Get all op players

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1ServerOpsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Get all op players
                List<OfflinePlayer> result = apiInstance.GetV1ServerOps(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.GetV1ServerOps: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1ServerOpsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Get all op players
    ApiResponse<List<OfflinePlayer>> response = apiInstance.GetV1ServerOpsWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.GetV1ServerOpsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

[**List&lt;OfflinePlayer&gt;**](OfflinePlayer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1serverops"></a>
# **PostV1ServerOps**
> void PostV1ServerOps (string? key = null, string? playerUuid = null)

Sets a specific player to Op

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1ServerOpsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new PlayerApi(config);
            var key = "key_example";  // string? |  (optional) 
            var playerUuid = "playerUuid_example";  // string? |  (optional) 

            try
            {
                // Sets a specific player to Op
                apiInstance.PostV1ServerOps(key, playerUuid);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PlayerApi.PostV1ServerOps: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1ServerOpsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Sets a specific player to Op
    apiInstance.PostV1ServerOpsWithHttpInfo(key, playerUuid);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PlayerApi.PostV1ServerOpsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **playerUuid** | **string?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

