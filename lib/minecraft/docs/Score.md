# Minecraft.Model.Score

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Entry** | **string** |  | [optional] 
**Value** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

