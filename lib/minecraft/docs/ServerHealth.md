# Minecraft.Model.ServerHealth

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Cpus** | **int** |  | [optional] 
**Uptime** | **long** |  | [optional] 
**TotalMemory** | **long** |  | [optional] 
**MaxMemory** | **long** |  | [optional] 
**FreeMemory** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

