# Minecraft.Api.EconomyApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**GetV1Economy**](EconomyApi.md#getv1economy) | **GET** /v1/economy | Economy plugin information |
| [**PostV1EconomyDebit**](EconomyApi.md#postv1economydebit) | **POST** /v1/economy/debit | Debit a player |
| [**PostV1EconomyPay**](EconomyApi.md#postv1economypay) | **POST** /v1/economy/pay | Pay a player |

<a name="getv1economy"></a>
# **GetV1Economy**
> Object GetV1Economy (string? key = null)

Economy plugin information

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class GetV1EconomyExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new EconomyApi(config);
            var key = "key_example";  // string? |  (optional) 

            try
            {
                // Economy plugin information
                Object result = apiInstance.GetV1Economy(key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling EconomyApi.GetV1Economy: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetV1EconomyWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Economy plugin information
    ApiResponse<Object> response = apiInstance.GetV1EconomyWithHttpInfo(key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling EconomyApi.GetV1EconomyWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1economydebit"></a>
# **PostV1EconomyDebit**
> Object PostV1EconomyDebit (string? key = null, string? uuid = null, decimal? amount = null)

Debit a player

Withdraws the provided amount out of the player's Vault

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1EconomyDebitExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new EconomyApi(config);
            var key = "key_example";  // string? |  (optional) 
            var uuid = "uuid_example";  // string? |  (optional) 
            var amount = 8.14D;  // decimal? |  (optional) 

            try
            {
                // Debit a player
                Object result = apiInstance.PostV1EconomyDebit(key, uuid, amount);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling EconomyApi.PostV1EconomyDebit: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1EconomyDebitWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Debit a player
    ApiResponse<Object> response = apiInstance.PostV1EconomyDebitWithHttpInfo(key, uuid, amount);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling EconomyApi.PostV1EconomyDebitWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **uuid** | **string?** |  | [optional]  |
| **amount** | **decimal?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1economypay"></a>
# **PostV1EconomyPay**
> Object PostV1EconomyPay (string? key = null, string? uuid = null, decimal? amount = null)

Pay a player

Deposits the provided amount into the player's Vault

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1EconomyPayExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new EconomyApi(config);
            var key = "key_example";  // string? |  (optional) 
            var uuid = "uuid_example";  // string? |  (optional) 
            var amount = 8.14D;  // decimal? |  (optional) 

            try
            {
                // Pay a player
                Object result = apiInstance.PostV1EconomyPay(key, uuid, amount);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling EconomyApi.PostV1EconomyPay: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1EconomyPayWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Pay a player
    ApiResponse<Object> response = apiInstance.PostV1EconomyPayWithHttpInfo(key, uuid, amount);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling EconomyApi.PostV1EconomyPayWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **uuid** | **string?** |  | [optional]  |
| **amount** | **decimal?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **500** | Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

