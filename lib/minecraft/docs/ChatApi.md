# Minecraft.Api.ChatApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**PostV1ChatBroadcast**](ChatApi.md#postv1chatbroadcast) | **POST** /v1/chat/broadcast | Send broadcast visible to those currently online. |
| [**PostV1ChatTell**](ChatApi.md#postv1chattell) | **POST** /v1/chat/tell | Send a message to a specific player. |

<a name="postv1chatbroadcast"></a>
# **PostV1ChatBroadcast**
> Object PostV1ChatBroadcast (string? key = null, string? message = null)

Send broadcast visible to those currently online.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1ChatBroadcastExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ChatApi(config);
            var key = "key_example";  // string? |  (optional) 
            var message = "message_example";  // string? |  (optional) 

            try
            {
                // Send broadcast visible to those currently online.
                Object result = apiInstance.PostV1ChatBroadcast(key, message);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ChatApi.PostV1ChatBroadcast: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1ChatBroadcastWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Send broadcast visible to those currently online.
    ApiResponse<Object> response = apiInstance.PostV1ChatBroadcastWithHttpInfo(key, message);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ChatApi.PostV1ChatBroadcastWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **message** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postv1chattell"></a>
# **PostV1ChatTell**
> Object PostV1ChatTell (string? key = null, string? message = null, string? playerUuid = null)

Send a message to a specific player.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Minecraft.Api;
using Minecraft.Client;
using Minecraft.Model;

namespace Example
{
    public class PostV1ChatTellExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new ChatApi(config);
            var key = "key_example";  // string? |  (optional) 
            var message = "message_example";  // string? |  (optional) 
            var playerUuid = "playerUuid_example";  // string? |  (optional) 

            try
            {
                // Send a message to a specific player.
                Object result = apiInstance.PostV1ChatTell(key, message, playerUuid);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ChatApi.PostV1ChatTell: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PostV1ChatTellWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Send a message to a specific player.
    ApiResponse<Object> response = apiInstance.PostV1ChatTellWithHttpInfo(key, message, playerUuid);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ChatApi.PostV1ChatTellWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **key** | **string?** |  | [optional]  |
| **message** | **string?** |  | [optional]  |
| **playerUuid** | **string?** |  | [optional]  |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

