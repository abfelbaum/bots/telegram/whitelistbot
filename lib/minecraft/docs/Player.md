# Minecraft.Model.Player

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uuid** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**Address** | **string** |  | [optional] 
**Port** | **int** |  | [optional] 
**Exhaustion** | **float** |  | [optional] 
**Exp** | **float** |  | [optional] 
**Whitelisted** | **bool** |  | [optional] 
**Banned** | **bool** |  | [optional] 
**Op** | **bool** |  | [optional] 
**Balance** | **double** |  | [optional] 
**Location** | **List&lt;double&gt;** |  | [optional] 
**Dimension** | **string** |  | [optional] 
**Health** | **double** |  | [optional] 
**Hunger** | **int** |  | [optional] 
**Saturation** | **float** |  | [optional] 
**Gamemode** | **string** |  | [optional] 
**LastPlayed** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

