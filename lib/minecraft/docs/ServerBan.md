# Minecraft.Model.ServerBan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Target** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 
**Reason** | **string** |  | [optional] 
**Expiration** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

