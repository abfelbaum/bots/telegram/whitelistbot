# Minecraft.Model.World

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**Uuid** | **string** |  | [optional] 
**Time** | **decimal** |  | [optional] 
**Storm** | **bool** |  | [optional] 
**Thundering** | **bool** |  | [optional] 
**GenerateStructures** | **bool** |  | [optional] 
**AllowAnimals** | **bool** |  | [optional] 
**AllowMonsters** | **bool** |  | [optional] 
**Difficulty** | **int** |  | [optional] 
**Environment** | **int** |  | [optional] 
**Seed** | **decimal** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

