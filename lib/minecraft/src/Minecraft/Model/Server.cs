/*
 * ServerTap
 *
 * ServerTap is a REST API for Bukkit/Spigot/PaperMC Minecraft servers.
 *
 * The version of the OpenAPI document: 0.4.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Minecraft.Client.OpenAPIDateConverter;

namespace Minecraft.Model
{
    /// <summary>
    /// Server
    /// </summary>
    [DataContract(Name = "Server")]
    public partial class Server : IEquatable<Server>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Server" /> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="motd">motd.</param>
        /// <param name="version">version.</param>
        /// <param name="bukkitVersion">bukkitVersion.</param>
        /// <param name="tps">tps.</param>
        /// <param name="health">health.</param>
        /// <param name="bannedIps">bannedIps.</param>
        /// <param name="bannedPlayers">bannedPlayers.</param>
        /// <param name="whitelistedPlayers">whitelistedPlayers.</param>
        /// <param name="maxPlayers">maxPlayers.</param>
        /// <param name="onlinePlayers">onlinePlayers.</param>
        public Server(string name = default(string), string motd = default(string), string version = default(string), string bukkitVersion = default(string), string tps = default(string), ServerHealth health = default(ServerHealth), List<ServerBan> bannedIps = default(List<ServerBan>), List<ServerBan> bannedPlayers = default(List<ServerBan>), List<Whitelist> whitelistedPlayers = default(List<Whitelist>), int maxPlayers = default(int), int onlinePlayers = default(int))
        {
            this.Name = name;
            this.Motd = motd;
            this._Version = version;
            this.BukkitVersion = bukkitVersion;
            this.Tps = tps;
            this.Health = health;
            this.BannedIps = bannedIps;
            this.BannedPlayers = bannedPlayers;
            this.WhitelistedPlayers = whitelistedPlayers;
            this.MaxPlayers = maxPlayers;
            this.OnlinePlayers = onlinePlayers;
        }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Motd
        /// </summary>
        [DataMember(Name = "motd", EmitDefaultValue = false)]
        public string Motd { get; set; }

        /// <summary>
        /// Gets or Sets _Version
        /// </summary>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        public string _Version { get; set; }

        /// <summary>
        /// Gets or Sets BukkitVersion
        /// </summary>
        [DataMember(Name = "bukkitVersion", EmitDefaultValue = false)]
        public string BukkitVersion { get; set; }

        /// <summary>
        /// Gets or Sets Tps
        /// </summary>
        [DataMember(Name = "tps", EmitDefaultValue = false)]
        public string Tps { get; set; }

        /// <summary>
        /// Gets or Sets Health
        /// </summary>
        [DataMember(Name = "health", EmitDefaultValue = false)]
        public ServerHealth Health { get; set; }

        /// <summary>
        /// Gets or Sets BannedIps
        /// </summary>
        [DataMember(Name = "bannedIps", EmitDefaultValue = false)]
        public List<ServerBan> BannedIps { get; set; }

        /// <summary>
        /// Gets or Sets BannedPlayers
        /// </summary>
        [DataMember(Name = "bannedPlayers", EmitDefaultValue = false)]
        public List<ServerBan> BannedPlayers { get; set; }

        /// <summary>
        /// Gets or Sets WhitelistedPlayers
        /// </summary>
        [DataMember(Name = "whitelistedPlayers", EmitDefaultValue = false)]
        public List<Whitelist> WhitelistedPlayers { get; set; }

        /// <summary>
        /// Gets or Sets MaxPlayers
        /// </summary>
        [DataMember(Name = "maxPlayers", EmitDefaultValue = false)]
        public int MaxPlayers { get; set; }

        /// <summary>
        /// Gets or Sets OnlinePlayers
        /// </summary>
        [DataMember(Name = "onlinePlayers", EmitDefaultValue = false)]
        public int OnlinePlayers { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class Server {\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Motd: ").Append(Motd).Append("\n");
            sb.Append("  _Version: ").Append(_Version).Append("\n");
            sb.Append("  BukkitVersion: ").Append(BukkitVersion).Append("\n");
            sb.Append("  Tps: ").Append(Tps).Append("\n");
            sb.Append("  Health: ").Append(Health).Append("\n");
            sb.Append("  BannedIps: ").Append(BannedIps).Append("\n");
            sb.Append("  BannedPlayers: ").Append(BannedPlayers).Append("\n");
            sb.Append("  WhitelistedPlayers: ").Append(WhitelistedPlayers).Append("\n");
            sb.Append("  MaxPlayers: ").Append(MaxPlayers).Append("\n");
            sb.Append("  OnlinePlayers: ").Append(OnlinePlayers).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Server);
        }

        /// <summary>
        /// Returns true if Server instances are equal
        /// </summary>
        /// <param name="input">Instance of Server to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Server input)
        {
            if (input == null)
            {
                return false;
            }
            return 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Motd == input.Motd ||
                    (this.Motd != null &&
                    this.Motd.Equals(input.Motd))
                ) && 
                (
                    this._Version == input._Version ||
                    (this._Version != null &&
                    this._Version.Equals(input._Version))
                ) && 
                (
                    this.BukkitVersion == input.BukkitVersion ||
                    (this.BukkitVersion != null &&
                    this.BukkitVersion.Equals(input.BukkitVersion))
                ) && 
                (
                    this.Tps == input.Tps ||
                    (this.Tps != null &&
                    this.Tps.Equals(input.Tps))
                ) && 
                (
                    this.Health == input.Health ||
                    (this.Health != null &&
                    this.Health.Equals(input.Health))
                ) && 
                (
                    this.BannedIps == input.BannedIps ||
                    this.BannedIps != null &&
                    input.BannedIps != null &&
                    this.BannedIps.SequenceEqual(input.BannedIps)
                ) && 
                (
                    this.BannedPlayers == input.BannedPlayers ||
                    this.BannedPlayers != null &&
                    input.BannedPlayers != null &&
                    this.BannedPlayers.SequenceEqual(input.BannedPlayers)
                ) && 
                (
                    this.WhitelistedPlayers == input.WhitelistedPlayers ||
                    this.WhitelistedPlayers != null &&
                    input.WhitelistedPlayers != null &&
                    this.WhitelistedPlayers.SequenceEqual(input.WhitelistedPlayers)
                ) && 
                (
                    this.MaxPlayers == input.MaxPlayers ||
                    this.MaxPlayers.Equals(input.MaxPlayers)
                ) && 
                (
                    this.OnlinePlayers == input.OnlinePlayers ||
                    this.OnlinePlayers.Equals(input.OnlinePlayers)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Name != null)
                {
                    hashCode = (hashCode * 59) + this.Name.GetHashCode();
                }
                if (this.Motd != null)
                {
                    hashCode = (hashCode * 59) + this.Motd.GetHashCode();
                }
                if (this._Version != null)
                {
                    hashCode = (hashCode * 59) + this._Version.GetHashCode();
                }
                if (this.BukkitVersion != null)
                {
                    hashCode = (hashCode * 59) + this.BukkitVersion.GetHashCode();
                }
                if (this.Tps != null)
                {
                    hashCode = (hashCode * 59) + this.Tps.GetHashCode();
                }
                if (this.Health != null)
                {
                    hashCode = (hashCode * 59) + this.Health.GetHashCode();
                }
                if (this.BannedIps != null)
                {
                    hashCode = (hashCode * 59) + this.BannedIps.GetHashCode();
                }
                if (this.BannedPlayers != null)
                {
                    hashCode = (hashCode * 59) + this.BannedPlayers.GetHashCode();
                }
                if (this.WhitelistedPlayers != null)
                {
                    hashCode = (hashCode * 59) + this.WhitelistedPlayers.GetHashCode();
                }
                hashCode = (hashCode * 59) + this.MaxPlayers.GetHashCode();
                hashCode = (hashCode * 59) + this.OnlinePlayers.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        public IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
