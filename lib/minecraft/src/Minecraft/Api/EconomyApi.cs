/*
 * ServerTap
 *
 * ServerTap is a REST API for Bukkit/Spigot/PaperMC Minecraft servers.
 *
 * The version of the OpenAPI document: 0.4.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mime;
using Minecraft.Client;

namespace Minecraft.Api
{

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IEconomyApiSync : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Economy plugin information
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        Object GetV1Economy(string? key = default(string?), int operationIndex = 0);

        /// <summary>
        /// Economy plugin information
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        ApiResponse<Object> GetV1EconomyWithHttpInfo(string? key = default(string?), int operationIndex = 0);
        /// <summary>
        /// Debit a player
        /// </summary>
        /// <remarks>
        /// Withdraws the provided amount out of the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        Object PostV1EconomyDebit(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0);

        /// <summary>
        /// Debit a player
        /// </summary>
        /// <remarks>
        /// Withdraws the provided amount out of the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        ApiResponse<Object> PostV1EconomyDebitWithHttpInfo(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0);
        /// <summary>
        /// Pay a player
        /// </summary>
        /// <remarks>
        /// Deposits the provided amount into the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        Object PostV1EconomyPay(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0);

        /// <summary>
        /// Pay a player
        /// </summary>
        /// <remarks>
        /// Deposits the provided amount into the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        ApiResponse<Object> PostV1EconomyPayWithHttpInfo(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0);
        #endregion Synchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IEconomyApiAsync : IApiAccessor
    {
        #region Asynchronous Operations
        /// <summary>
        /// Economy plugin information
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        System.Threading.Tasks.Task<Object> GetV1EconomyAsync(string? key = default(string?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// Economy plugin information
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> GetV1EconomyWithHttpInfoAsync(string? key = default(string?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        /// <summary>
        /// Debit a player
        /// </summary>
        /// <remarks>
        /// Withdraws the provided amount out of the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        System.Threading.Tasks.Task<Object> PostV1EconomyDebitAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// Debit a player
        /// </summary>
        /// <remarks>
        /// Withdraws the provided amount out of the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> PostV1EconomyDebitWithHttpInfoAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        /// <summary>
        /// Pay a player
        /// </summary>
        /// <remarks>
        /// Deposits the provided amount into the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        System.Threading.Tasks.Task<Object> PostV1EconomyPayAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));

        /// <summary>
        /// Pay a player
        /// </summary>
        /// <remarks>
        /// Deposits the provided amount into the player&#39;s Vault
        /// </remarks>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> PostV1EconomyPayWithHttpInfoAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IEconomyApi : IEconomyApiSync, IEconomyApiAsync
    {

    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class EconomyApi : IEconomyApi
    {
        private Minecraft.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="EconomyApi"/> class.
        /// </summary>
        /// <returns></returns>
        public EconomyApi() : this((string)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EconomyApi"/> class.
        /// </summary>
        /// <returns></returns>
        public EconomyApi(string basePath)
        {
            this.Configuration = Minecraft.Client.Configuration.MergeConfigurations(
                Minecraft.Client.GlobalConfiguration.Instance,
                new Minecraft.Client.Configuration { BasePath = basePath }
            );
            this.Client = new Minecraft.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Minecraft.Client.ApiClient(this.Configuration.BasePath);
            this.ExceptionFactory = Minecraft.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EconomyApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public EconomyApi(Minecraft.Client.Configuration configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Configuration = Minecraft.Client.Configuration.MergeConfigurations(
                Minecraft.Client.GlobalConfiguration.Instance,
                configuration
            );
            this.Client = new Minecraft.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Minecraft.Client.ApiClient(this.Configuration.BasePath);
            ExceptionFactory = Minecraft.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EconomyApi"/> class
        /// using a Configuration object and client instance.
        /// </summary>
        /// <param name="client">The client interface for synchronous API access.</param>
        /// <param name="asyncClient">The client interface for asynchronous API access.</param>
        /// <param name="configuration">The configuration object.</param>
        public EconomyApi(Minecraft.Client.ISynchronousClient client, Minecraft.Client.IAsynchronousClient asyncClient, Minecraft.Client.IReadableConfiguration configuration)
        {
            if (client == null) throw new ArgumentNullException("client");
            if (asyncClient == null) throw new ArgumentNullException("asyncClient");
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Client = client;
            this.AsynchronousClient = asyncClient;
            this.Configuration = configuration;
            this.ExceptionFactory = Minecraft.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// The client for accessing this underlying API asynchronously.
        /// </summary>
        public Minecraft.Client.IAsynchronousClient AsynchronousClient { get; set; }

        /// <summary>
        /// The client for accessing this underlying API synchronously.
        /// </summary>
        public Minecraft.Client.ISynchronousClient Client { get; set; }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public string GetBasePath()
        {
            return this.Configuration.BasePath;
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Minecraft.Client.IReadableConfiguration Configuration { get; set; }

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public Minecraft.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Economy plugin information 
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        public Object GetV1Economy(string? key = default(string?), int operationIndex = 0)
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = GetV1EconomyWithHttpInfo(key);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Economy plugin information 
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        public Minecraft.Client.ApiResponse<Object> GetV1EconomyWithHttpInfo(string? key = default(string?), int operationIndex = 0)
        {
            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.GetV1Economy";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = this.Client.Get<Object>("/v1/economy", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetV1Economy", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Economy plugin information 
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        public async System.Threading.Tasks.Task<Object> GetV1EconomyAsync(string? key = default(string?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = await GetV1EconomyWithHttpInfoAsync(key, operationIndex, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Economy plugin information 
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        public async System.Threading.Tasks.Task<Minecraft.Client.ApiResponse<Object>> GetV1EconomyWithHttpInfoAsync(string? key = default(string?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {

            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.GetV1Economy";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.GetAsync<Object>("/v1/economy", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetV1Economy", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Debit a player Withdraws the provided amount out of the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        public Object PostV1EconomyDebit(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0)
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = PostV1EconomyDebitWithHttpInfo(key, uuid, amount);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Debit a player Withdraws the provided amount out of the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        public Minecraft.Client.ApiResponse<Object> PostV1EconomyDebitWithHttpInfo(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0)
        {
            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }
            if (uuid != null)
            {
                localVarRequestOptions.FormParameters.Add("uuid", Minecraft.Client.ClientUtils.ParameterToString(uuid)); // form parameter
            }
            if (amount != null)
            {
                localVarRequestOptions.FormParameters.Add("amount", Minecraft.Client.ClientUtils.ParameterToString(amount)); // form parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.PostV1EconomyDebit";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = this.Client.Post<Object>("/v1/economy/debit", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("PostV1EconomyDebit", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Debit a player Withdraws the provided amount out of the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        public async System.Threading.Tasks.Task<Object> PostV1EconomyDebitAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = await PostV1EconomyDebitWithHttpInfoAsync(key, uuid, amount, operationIndex, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Debit a player Withdraws the provided amount out of the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        public async System.Threading.Tasks.Task<Minecraft.Client.ApiResponse<Object>> PostV1EconomyDebitWithHttpInfoAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {

            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }
            if (uuid != null)
            {
                localVarRequestOptions.FormParameters.Add("uuid", Minecraft.Client.ClientUtils.ParameterToString(uuid)); // form parameter
            }
            if (amount != null)
            {
                localVarRequestOptions.FormParameters.Add("amount", Minecraft.Client.ClientUtils.ParameterToString(amount)); // form parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.PostV1EconomyDebit";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.PostAsync<Object>("/v1/economy/debit", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("PostV1EconomyDebit", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Pay a player Deposits the provided amount into the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>Object</returns>
        public Object PostV1EconomyPay(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0)
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = PostV1EconomyPayWithHttpInfo(key, uuid, amount);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Pay a player Deposits the provided amount into the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object</returns>
        public Minecraft.Client.ApiResponse<Object> PostV1EconomyPayWithHttpInfo(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0)
        {
            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }
            if (uuid != null)
            {
                localVarRequestOptions.FormParameters.Add("uuid", Minecraft.Client.ClientUtils.ParameterToString(uuid)); // form parameter
            }
            if (amount != null)
            {
                localVarRequestOptions.FormParameters.Add("amount", Minecraft.Client.ClientUtils.ParameterToString(amount)); // form parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.PostV1EconomyPay";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = this.Client.Post<Object>("/v1/economy/pay", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("PostV1EconomyPay", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Pay a player Deposits the provided amount into the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of Object</returns>
        public async System.Threading.Tasks.Task<Object> PostV1EconomyPayAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            Minecraft.Client.ApiResponse<Object> localVarResponse = await PostV1EconomyPayWithHttpInfoAsync(key, uuid, amount, operationIndex, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Pay a player Deposits the provided amount into the player&#39;s Vault
        /// </summary>
        /// <exception cref="Minecraft.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="key"> (optional)</param>
        /// <param name="uuid"> (optional)</param>
        /// <param name="amount"> (optional)</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (Object)</returns>
        public async System.Threading.Tasks.Task<Minecraft.Client.ApiResponse<Object>> PostV1EconomyPayWithHttpInfoAsync(string? key = default(string?), string? uuid = default(string?), decimal? amount = default(decimal?), int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {

            Minecraft.Client.RequestOptions localVarRequestOptions = new Minecraft.Client.RequestOptions();

            string[] _contentTypes = new string[] {
                "application/x-www-form-urlencoded"
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json"
            };

            var localVarContentType = Minecraft.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Minecraft.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            if (key != null)
            {
                localVarRequestOptions.HeaderParameters.Add("key", Minecraft.Client.ClientUtils.ParameterToString(key)); // header parameter
            }
            if (uuid != null)
            {
                localVarRequestOptions.FormParameters.Add("uuid", Minecraft.Client.ClientUtils.ParameterToString(uuid)); // form parameter
            }
            if (amount != null)
            {
                localVarRequestOptions.FormParameters.Add("amount", Minecraft.Client.ClientUtils.ParameterToString(amount)); // form parameter
            }

            localVarRequestOptions.Operation = "EconomyApi.PostV1EconomyPay";
            localVarRequestOptions.OperationIndex = operationIndex;


            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.PostAsync<Object>("/v1/economy/pay", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("PostV1EconomyPay", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

    }
}
