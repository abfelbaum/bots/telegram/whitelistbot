/*
 * ServerTap
 *
 * ServerTap is a REST API for Bukkit/Spigot/PaperMC Minecraft servers.
 *
 * The version of the OpenAPI document: 0.4.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Minecraft.Api;
using Minecraft.Model;
using Minecraft.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Minecraft.Test.Model
{
    /// <summary>
    ///  Class for testing World
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class WorldTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for World
        //private World instance;

        public WorldTests()
        {
            // TODO uncomment below to create an instance of World
            //instance = new World();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of World
        /// </summary>
        [Fact]
        public void WorldInstanceTest()
        {
            // TODO uncomment below to test "IsType" World
            //Assert.IsType<World>(instance);
        }


        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Fact]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Uuid'
        /// </summary>
        [Fact]
        public void UuidTest()
        {
            // TODO unit test for the property 'Uuid'
        }
        /// <summary>
        /// Test the property 'Time'
        /// </summary>
        [Fact]
        public void TimeTest()
        {
            // TODO unit test for the property 'Time'
        }
        /// <summary>
        /// Test the property 'Storm'
        /// </summary>
        [Fact]
        public void StormTest()
        {
            // TODO unit test for the property 'Storm'
        }
        /// <summary>
        /// Test the property 'Thundering'
        /// </summary>
        [Fact]
        public void ThunderingTest()
        {
            // TODO unit test for the property 'Thundering'
        }
        /// <summary>
        /// Test the property 'GenerateStructures'
        /// </summary>
        [Fact]
        public void GenerateStructuresTest()
        {
            // TODO unit test for the property 'GenerateStructures'
        }
        /// <summary>
        /// Test the property 'AllowAnimals'
        /// </summary>
        [Fact]
        public void AllowAnimalsTest()
        {
            // TODO unit test for the property 'AllowAnimals'
        }
        /// <summary>
        /// Test the property 'AllowMonsters'
        /// </summary>
        [Fact]
        public void AllowMonstersTest()
        {
            // TODO unit test for the property 'AllowMonsters'
        }
        /// <summary>
        /// Test the property 'Difficulty'
        /// </summary>
        [Fact]
        public void DifficultyTest()
        {
            // TODO unit test for the property 'Difficulty'
        }
        /// <summary>
        /// Test the property 'Environment'
        /// </summary>
        [Fact]
        public void EnvironmentTest()
        {
            // TODO unit test for the property 'Environment'
        }
        /// <summary>
        /// Test the property 'Seed'
        /// </summary>
        [Fact]
        public void SeedTest()
        {
            // TODO unit test for the property 'Seed'
        }

    }

}
